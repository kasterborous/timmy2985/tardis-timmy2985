local PART={}
PART.ID = "timmyblackswitch"
PART.Name = "timmyblackswitch"
PART.Model = "models/Timmy2985/Tardis/Interior/blackswitch.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2


if SERVER then
	function PART:Use(activator)
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_switch_original.wav" ))
	end
end

TARDIS:AddPart(PART,e)