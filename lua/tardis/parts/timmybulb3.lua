local PART={}
PART.ID = "timmybulb3"
PART.Name = "timmybulb3"
PART.Model = "models/Timmy2985/Tardis/Interior/bulb3.mdl"
PART.AutoSetup = true

if CLIENT then
	function PART:Think()
		local switch = TARDIS:GetPart(self.interior,"timmytinytinyswitch14")
		if ( switch:GetOn() ) then
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1on")
		else
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1off")
		end
	end
end

TARDIS:AddPart(PART,e)