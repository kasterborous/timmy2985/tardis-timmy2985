local PART={}
PART.ID = "timmybulb14"
PART.Name = "timmybulb14"
PART.Model = "models/Timmy2985/Tardis/Interior/bulb14.mdl"
PART.AutoSetup = true

if CLIENT then
	function PART:Think()
		local exterior=self.exterior
		local switch = TARDIS:GetPart(self.interior,"timmysmallswitch5")
		if ( switch:GetOn() ) then
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1on")
				if (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
					self:SetMaterial("models/Timmy2985/Tardis/Interior/animatedbulbsblink")
				else
					self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1on")
				end
		else
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1off")
		end
	end
end

TARDIS:AddPart(PART,e)