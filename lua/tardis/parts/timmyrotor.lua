local PART={}
PART.ID = "timmyrotor"
PART.Name = "timmyrotor"
PART.Model = "models/Timmy2985/Tardis/Interior/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.UseTransparencyFix = true

if CLIENT then
	function PART:Initialize()
	self.rotor={}
	self.rotor.pos=0
	self.rotor.mode=1
	self.rotorspin={}
	self.rotorspin.pos=0
	self.rotorspin.mode=1
	end

	function PART:Think()
			if self.rotorspin.pos==0 then
				self.rotorspin.pos=1
			elseif self.rotorspin.pos==1 then
				self.rotorspin.pos=0
			end
				
			self.rotorspin.pos=math.Approach( self.rotorspin.pos, self.rotorspin.mode, FrameTime()*0.05 )
			self:SetPoseParameter( "rotorspin", self.rotorspin.pos )

		local exterior=self.exterior
		if (self.rotor.pos>0 and not exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) or (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
			if self.rotor.pos==0 then
				self.rotor.pos=1
			elseif self.rotor.pos==1 and (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
				self.rotor.pos=0
			end
				
			self.rotor.pos=math.Approach( self.rotor.pos, self.rotor.mode, FrameTime()*0.2325 )
			self:SetPoseParameter( "rotor", self.rotor.pos )
		end
	end
end

TARDIS:AddPart(PART)