local PART={}
PART.ID = "timmybigleverswitch3"
PART.Name = "timmybigleverswitch3"
PART.Model = "models/Timmy2985/Tardis/Interior/bigleverswitch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2

if SERVER then
	function PART:Use()
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_lever.wav" ))
	end
end

TARDIS:AddPart(PART,e)