local PART={}
PART.ID = "timmyfloorlights"
PART.Name = "timmyfloorlights"
PART.Model = "models/Timmy2985/Tardis/Interior/floorlights.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Think()
		local exterior=self.exterior
		mat=Material("models/Timmy2985/Tardis/Interior/floorlightsoff")
			if exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex") or exterior:GetData("float") then
				self:SetMaterial("models/Timmy2985/Tardis/Interior/floorlightson")
			else
				self:SetMaterial("models/Timmy2985/Tardis/Interior/floorlightsoff")
			end
	end
end

TARDIS:AddPart(PART)