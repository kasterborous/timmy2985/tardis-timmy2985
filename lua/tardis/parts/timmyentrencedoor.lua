local PART={}
PART.ID = "timmyentrencedoor"
PART.Name = "timmyentrencedoor"
PART.Model = "models/Timmy2985/Tardis/Interior/entrencedoor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.5


if SERVER then
	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:Use()
		sound.Play("timmy2985/tardis/sounds/timmy_doors.wav", self:LocalToWorld(Vector(285,0,0)))
		if ( self:GetOn() ) then
			self:Collide( true )
		else
			self:DontCollide( true )
		end
	end

	function PART:Toggle( bEnable, ply )
		if ( bEnable ) then
			self:SetOn( true )
			self:DontCollide( true )
			sound.Play("timmy2985/tardis/sounds/timmy_doors.wav", self:LocalToWorld(Vector(285,0,0)))
		else
			self:SetOn( false )
			self:Collide( true )
			sound.Play("timmy2985/tardis/sounds/timmy_doors.wav", self:LocalToWorld(Vector(285,0,0)))
		end
	end
end

TARDIS:AddPart(PART,e)